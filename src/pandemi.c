/* pandemi.c
 * Auteur: Alexandre Côté Cyr
 * Code Permanent: COTA07049302
 *
 * Ce programme calcule l'évolution d'une pandémie à partir d'une grille fournie.
 * Le programme affiche tous les jours l'évolution de la grille
 * Le nombre de jours est passé en argument et la grille de départ est passée par STDIN
 *
 * usage: pandemi.c n_jours
 * 			string_grille
 */

//Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB_RANGEES 20
#define NB_COLONNES 40

char adjacent(char grille[NB_RANGEES][NB_COLONNES], int rangee, int colonne);
void afficherGrille(char grille[NB_RANGEES][NB_COLONNES]);
void avancerJour(char grille[NB_RANGEES][NB_COLONNES]);

//Main
int main(int argc, char* argv[]) {

	long nbJours;
	char* ptr;
	char grillePop[NB_RANGEES][NB_COLONNES];
	char caracActuel;
	if(argc > 2) {
		puts("Erreur: Attendu un seul argument: le nombre de jours à simuler");
		return 1;
	} else if(argc == 2) {
		nbJours = strtol(argv[1], &ptr, 10);
		if(nbJours < 0 || nbJours >100 || strlen(ptr) != 0) {
			puts("Erreur: Le nombre de jours à simuler doit être entre 0 et 100.");
			return 2;
		}
	} else {
		nbJours = 0;
	}

	//Lit la grille à partir de STDIN
	for(int i = 0; i < NB_RANGEES; i++) {
		for(int j = 0; j < NB_COLONNES; j++) {
			//Ignore les espaces et retour de lignes
			fscanf(stdin, " ");
			//Lit les caractères un à un et vérifie qu'ils sont valides
			caracActuel = fgetc(stdin);
			if (caracActuel == EOF) {
				printf("Erreur: Caractère `EOF` inattendu, attendu `H`, `X` ou `.`.\n");
				return 3;
			} else if(strchr("X.H", caracActuel) == NULL) {
				printf("Erreur: Caractère `%c` inattendu, attendu `H`, `X` ou `.`.\n", caracActuel);
				return 3;
			} else {
				grillePop[i][j] = caracActuel;
			}
		}
	}
	//Affiche la grille pour le nombre de jours voulus. Le jour 0 est toujours affiché
	printf("Jour 0\n");
	afficherGrille(grillePop);
	for(int i = 1; i <= nbJours; i++) {
		avancerJour(grillePop);
		printf("Jour %d\n", i);
		afficherGrille(grillePop);
	}
	return 0;
}

/* Cette fonction calcule le nombre de chaque type de cellules qui sont adjacentes à la cellule à la position indiquée
 *
 * 1- Une zone vide devient peuplée si elle a 3 voisins peuplés.
 *    Si la majorité de ces voisins sont infectés, elle l'est aussi. Sinon elle est saine.
 *
 * 2- Une zone peuplée reste peuplée si elle a 2 ou 3 voisins peuplés.
 *    Si la majorité de ces voisins sont infectés, elle le devient aussi. Sinon est ne change pas d'état.
 *
 * 3- Une sone peuplée devient vide si elle a moins de 2 ou plus de 3 voisins peuplés. *
 *
 * @param grille: La grille de population comme elle était au début de la journée
 * @param rangee: Le numéro de rangée de la cellule actuelle
 * @param colonne: Le numéro de colonne de la cellule actuelle
 * @return: l'état de la cellule actuelle à la fin de la journée
 */
char adjacent(char grille[NB_RANGEES][NB_COLONNES], int rangee, int colonne) {
	int nbSain = 0;
	int nbInfect = 0;
	char etat;
	//Itère à travers les voisins de la cellule courante pour voir leur état actuel.
	//Passe tout de suite au prochain si l'index est en dehors de la grille ou est la cellule actuelle
	for(int i = rangee-1; i <= rangee+1; i++) {
		if(i < 0 || i >= NB_RANGEES) {
			continue;
		}
		for(int j = colonne-1; j <= colonne+1; j++) {
			if(j < 0 || j >= NB_COLONNES || (i == rangee && j == colonne)) {
				continue;
			}
			if(grille[i][j] == 'X') {
				nbInfect++;
			} else if(grille[i][j] == 'H') {
				nbSain++;
			}

		}
	}
	if(grille[rangee][colonne] == '.') {
		if(nbSain + nbInfect == 3) {
			if(nbInfect > nbSain) {
				etat = 'X';
			} else {
				etat = 'H';
			}
		} else {
			etat = '.';
		}
	} else {
		if(nbSain + nbInfect == 2 || nbSain + nbInfect == 3) {
			if(nbInfect > nbSain) {
				etat = 'X';
			} else {
				etat = grille[rangee][colonne];

			}
		} else {
			etat = '.';
		}
	}
	return etat;
}

/* Cette fonction affiche une grille caractère par caractère vers STDOUT
 * @param grille: la grille à afficher
 */
void afficherGrille(char grille[NB_RANGEES][NB_COLONNES]) {
	for(int i = 0; i < NB_RANGEES; i++) {
		for(int j = 0; j < NB_COLONNES; j++) {
			putchar(grille[i][j]);
		}
		putchar('\n');
	}
}

/* Cette fonction prend une grille et la modifie pour qu'elle contienne sont état après une journée
 * La grille est modifiée en place, donc la fonction ne retourne rien
 *
 * @param grille: grille à faire avancer
 */
void avancerJour(char grille[NB_RANGEES][NB_COLONNES]) {
	//Fait une copie de la grille originale
	char grilleCopie[NB_RANGEES][NB_COLONNES] = {0};
	memcpy(grilleCopie, grille, NB_RANGEES * NB_COLONNES * sizeof(char));
	for(int i = 0; i < NB_RANGEES; i++) {
		for(int j = 0; j < NB_COLONNES; j++) {
			grille[i][j] = adjacent(grilleCopie, i, j);
		}
	}
}

